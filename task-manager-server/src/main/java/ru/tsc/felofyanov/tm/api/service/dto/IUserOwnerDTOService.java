package ru.tsc.felofyanov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.dto.IUserOwnerDTORepository;
import ru.tsc.felofyanov.tm.dto.model.AbstractWbsDTO;
import ru.tsc.felofyanov.tm.enumerated.Status;

public interface IUserOwnerDTOService<M extends AbstractWbsDTO> extends IUserOwnerDTORepository<M>, IServiceDTO<M> {

    @NotNull
    M updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    M updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    M changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);
}
