package ru.tsc.felofyanov.tm.exception.field;

public final class FIOEmptyException extends AbstractFieldException {
    public FIOEmptyException() {
        super("Error! Firstname, Lastname, Middlename must be filled in...");
    }
}
